# Plugin de "comptage"

Cette extension permet d'effectuer des comptages horodatés et d'analyser les résultats.

Exemples d'utilisation : 
- pour une association de cyclistes urbains militants : dénombrer les types de véhicules empruntant une rue (véhicules motorisés, vélos, piétons, ...)
- compter les participants à une manifestation ou à un évenement, tout en mesurant l'affluence selon l'heure

# Installation

- Télécharger le fichier comptages.tar.gz et l'ajouter dans le répértoire garradin/plugins.
- Installer le plugin via le menu "Configuration > Extensions" de Garradin.

# Utilisation
- Cliquer sur 'comptages' dans le menu Garradin (visible aux membres ayant le droit d'accès au wiki)
- Cliquer sur le bouton 'ajouter un comptage' pour décrire le lieu où le comptage sera effectué, et si besoin modifier les catégories à compter
- Pour commencer à compter, cliquer/appuyer sur l'icone "robot" 🤖
- Effectuer le comptage en cliquant/appuyant sur la catégorie adéquate
- Pour enregistrer l'heure de fin du comptage, quitter la page (en cliquant par exemple n'importe où sur le menu de Garradin en haut de l'écran) ou appuyer sur le bouton "arrêt / pause".
- Il est possible d'effectuer plusieurs comptages successifs au même endroit. (par exemple : en heure de pointe, puis en milieu de journée, puis le soir, etc.)

# Analyse des résultats
On peut ensuite, pour tous les comptages, y compris ceux des autres adhérents :
- consulter des statistiques avec l’icône "graphique" 📈 
- exporter les données brutes avec l’icône "sacoche" 💼, en vue de les analyser avec d'autres outils (tableur, ...)

# Ergonomie
- Les écrans de consultation sont plus faciles à lire depuis un écran d'ordinateur
- L'écran pour effectuer les comptages est optimisé pour être utilisé depuis un téléphone ou une tablette

# Paramètrage
Un écran de paramètrage accessible uniquement aux membres ayant le droit de modifier la configuration permet de modifier les catégories de comptages proposées par défaut.

Accès via Configuration > Extensions > Comptages > Configurer

# Droits d'accès

- Les écrans sont accessibles en consultation uniquement pour les membres ayant le droit d'accès aux documents.
- Les membres peuvent consulter les données de leurs comptages ainsi que celles des comptages des autres membres.
- Le droit de consultation de documents suffit pour pouvoir créér, modifier et supprimer ses propres comptages.
- Chaque comptage est associé au membre l'ayant créé. Ce membre est le seul à pouvoir "compter", modifier la description ou supprimer son comptage.
- Les membres ayant le droit d'administration des documents peuvent modifier les descriptions ou effacer tous les comptages.

