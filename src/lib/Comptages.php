<?php

namespace Garradin\Plugin\comptages;

use Garradin\Plugin;
use Garradin\DB;
use Garradin\Config;
use Garradin\Membres\Session;
use Garradin\UserException;
use Garradin\Utils;
use Garradin\CSV;

// TODO Visualiser - Exporter (avec intervalles) - Modifier - Supprimer

class Comptages
{

	static public function ajouterComptage($id_session_comptage, $type_comptage) {

		$data = [];

		$data['timestamp_comptage'] = date("Y-m-d H:i:s");

		$data['id_session_comptage'] = $id_session_comptage;

		$data['type_comptage'] = $type_comptage;

		$db = DB::getInstance();

		$db->insert('plugin_comptages_comptages', $data); 

	}


	static public function addSession(&$data = [], $types_comptages = [])
	{
	        if (empty($data['lieu'])) 
		{
			throw new UserException('La description du lieu du comptage est obligatoire');
		}

		$db = DB::getInstance();

        	$session = Session::getInstance();
		$user = $session->getUser();
        	$id = $user->id;
		
		$data['id_session_comptage'] = $db->firstColumn('SELECT MAX(id_session_comptage) + 1 FROM plugin_comptages_sessions;') ?: 1;
		$data['id_membre_compteur'] = $id;
		$data['timestamp_date_creation'] = date("Y-m-d H:i:s");


		foreach ($types_comptages as $type => $description) {
			$data_type = [];
			$data_type['id_session_comptage'] = $data['id_session_comptage'];
			$data_type['type_comptage'] = $type;
			$data_type['description'] = $description;
			$db->insert('plugin_comptages_types', $data_type);
		}


		$db->insert('plugin_comptages_sessions', $data);

	}

	static public function editSession($id_session_comptage, $data, $types_comptages)
	{
	        if (empty($data['lieu'])) 
		{
			throw new UserException('La description du lieu du comptage est obligatoire');
		}

		$db = DB::getInstance();

		$success = true;


		foreach ($types_comptages as $type => $description) {
			$success &= $db->update('plugin_comptages_types', 
			['description' => $description], 
			'id_session_comptage=:id_session_comptage and type_comptage=:type_comptage',
			['id_session_comptage' => (int)$id_session_comptage,
			 'type_comptage' => (int)$type]);
		}




		$success &= $db->update('plugin_comptages_sessions', $data, $db->where('id_session_comptage', (int)$id_session_comptage));

		return($success);
	}	



	static public function getList()
	{
		$champ_id = Config::getInstance()->get('champ_identite');

		

		return DB::getInstance()->get('SELECT timestamp_date_creation, id_session_comptage, id_membre_compteur, lieu, commentaires, '.$champ_id.'
			 as nom_membre
			 FROM plugin_comptages_sessions, membres
			where plugin_comptages_sessions.id_membre_compteur = membres.id
			ORDER BY timestamp_date_creation;');
	}

	static public function getTypesComptages($id_session_comptage)
	{
		return DB::getInstance()->get('SELECT type_comptage, description
		FROM plugin_comptages_types
		where id_session_comptage = :id_session_comptage
		order by type_comptage',
		['id_session_comptage' => $id_session_comptage]);

	}


	static public function getSession($id_session_comptage)
	{
		return DB::getInstance()->first('SELECT *
		FROM plugin_comptages_sessions
		where id_session_comptage = :id_session_comptage',
		['id_session_comptage' => $id_session_comptage]);

	}


	static public function calculerTotaux($types_comptages, $horodatages)
	{

		$intervalle_en_cours = 0;

		$intervalles = array();

		$init_ligne_totaux = array();

		foreach ($types_comptages as $type_comptage) {
			if ($type_comptage->{'description'} != "") {
				$init_ligne_totaux[$type_comptage->{'type_comptage'}] = 0;
			}		
		}	


		$totaux_par_type = $init_ligne_totaux;


		$unixtimemini = PHP_INT_MAX;
		$unixtimemaxi = 1;

		foreach ($horodatages as $horodatage) {

			$type = $horodatage->{'type_comptage'};

			$unixtime = strtotime($horodatage->{'timestamp_comptage'});

			if ($unixtime < $unixtimemini) {
				$unixtimemini = $unixtime;
			}


			if ($type > 0) {

				$totaux_par_type[$type]++;

				if ($unixtime > $unixtimemaxi) {
					$unixtimemaxi = $unixtime;
				}

			} elseif ($type == 0) {
				# enregistrement d'un intervalle (ouverture de la page de comptage)
				if (self::sommeHorodatages($totaux_par_type) > 0) {
					# on ne créé un nouvel intervalle que si le précédent n'est pas vide					
					# on commence par enregistrer les timestamps de début de l'intervalle précédent
				
					if ($unixtimemini == $unixtimemaxi) {
						# cas d'un comptage ultra court de 0s ! on arrondit à 1s pour éviter les divisions par zéro
						$unixtimemaxi = $unixtimemini + 1;
					}
					array_push($intervalles, self::miseEnFormeIntervalle($unixtimemini, $unixtimemaxi, $totaux_par_type));

					$totaux_par_type = $init_ligne_totaux;
				}				
				$unixtimemini = $unixtime;
				$unixtimemaxi = 1;
				
			}


		}

		# comme on n'a pas d'horodatage de fin, on enregistre ici le dernier intervalle
		if ($type > 0) {
			# sauf si le dernier horodatage est de type "démarrage comptage"		
			if ($unixtimemini == $unixtimemaxi) {
				# cas d'un comptage ultra court de 0s ! on arrondit à 1s pour éviter les divisions par zéro
				$unixtimemaxi = $unixtimemini + 1;
			}
			array_push($intervalles, self::miseEnFormeIntervalle($unixtimemini, $unixtimemaxi, $totaux_par_type));
		}

		$duree_totale_secondes = 0;
		$grands_totaux_par_type = $init_ligne_totaux;	

		foreach ($intervalles as $intervalle)
		{
			$duree_totale_secondes += $intervalle['duree_secondes'];

			foreach ($intervalle['totaux'] as $type => $nombre) 
			{
				$grands_totaux_par_type[$type] += $nombre;


			}

		}

		if (self::sommeHorodatages($grands_totaux_par_type) == 0) {
			# c'est le cas si on a juste démarré des comptages, sans jamais compter
			return false;
		}

		$grands_pourcentages_par_type = array();

		$grands_taux_par_heure = array();

		foreach ($grands_totaux_par_type as $type => $total) {

			$grands_pourcentages_par_type[$type] = round(($grands_totaux_par_type[$type]*100)/self::sommeHorodatages($grands_totaux_par_type),2);

			$grands_taux_par_heure[$type] = round(($grands_totaux_par_type[$type]/$duree_totale_secondes)*3600,0);

		}


		array_push($intervalles,['debut_unix' => 0, 
				'fin_unix' => 0, 
				'duree_secondes' => $duree_totale_secondes,
				'datetime_debut' => 'Total',
				'datetime_fin' => '',
				'duree_HMS' => self::secondesToHMS($duree_totale_secondes),
				'totaux' => $grands_totaux_par_type,
				'total_horodatages' => self::sommeHorodatages($grands_totaux_par_type),
				'pourcentages' => $grands_pourcentages_par_type,
				'taux_horaires' => $grands_taux_par_heure,
				'taux_horaire_total' => round((self::sommeHorodatages($grands_totaux_par_type)/$duree_totale_secondes)*3600,0)]);


		return($intervalles);

	}

	static public function sommeHorodatages($totaux) {
		$total = 0;
		foreach ($totaux as $type => $nombre)
		{
			$total += $nombre;
		}
		return $total;
	}
			
	static public function miseEnFormeIntervalle($unixtimemini, $unixtimemaxi, $totaux_par_type) {

		$total_horodatages = self::sommeHorodatages($totaux_par_type);

		$duree_secondes = $unixtimemaxi - $unixtimemini;

		$pourcentages_par_type = array();

		$taux_par_heure = array();

		foreach ($totaux_par_type as $type => $total) {

			$pourcentages_par_type[$type] = round(($totaux_par_type[$type]*100)/$total_horodatages,2);

			$taux_par_heure[$type] = round(($totaux_par_type[$type]/$duree_secondes)*3600,0);

		}

		return (
				['debut_unix' => $unixtimemini, 
				'fin_unix' => $unixtimemaxi, 
				'duree_secondes' => $duree_secondes,
				'datetime_debut' => date("Y-m-d H:i:s", $unixtimemini),
				'datetime_fin' => date("Y-m-d H:i:s", $unixtimemaxi),
				'duree_HMS' => self::secondesToHMS($duree_secondes),
				'totaux' => $totaux_par_type,
				'total_horodatages' => $total_horodatages,
				'pourcentages' => $pourcentages_par_type,
				'taux_horaires' => $taux_par_heure,
				'taux_horaire_total' => round(($total_horodatages/$duree_secondes)*3600,0)]);
	}

	static public function secondesToHMS($total_secondes) {
		$heures = floor($total_secondes / 3600);
		$minutes = floor(($total_secondes / 60) % 60);
		$secondes = $total_secondes % 60;

		if ($heures > 0) {
			return (sprintf("%01dh%02dmn%02s", $heures, $minutes, $secondes));
		} else {
			return (sprintf("%01dmn%02ds", $minutes, $secondes));
		}
	}


	static public function getComptages($id_session_comptage)
	{
		return DB::getInstance()->get('SELECT *
		FROM plugin_comptages_comptages
		where id_session_comptage = :id_session_comptage
		order by timestamp_comptage, type_comptage',
		['id_session_comptage' => $id_session_comptage]);

	}


	static public function export_rawdata_ODS($id_session_comptage)
	{
		$db = DB::getInstance();	

		$champs = ['#comptage','lieu','horodatage','type', 'description'];

		$res = $db->iterate("select com.id_session_comptage, ses.lieu,
				com.timestamp_comptage, 
				com.type_comptage, ifnull(typ.description,'Ouverture page de comptage') as description

				from plugin_comptages_sessions ses, plugin_comptages_comptages com
				left join plugin_comptages_types typ
				on typ.type_comptage = com.type_comptage
				and typ.id_session_comptage = com.id_session_comptage

				where com.id_session_comptage = :id_session_comptage
				and com.id_session_comptage = ses.id_session_comptage",
				['id_session_comptage' => $id_session_comptage]);
		
		$nom_fichier = sprintf('Export comptage numero %s - %s - %s', $id_session_comptage, Config::getInstance()->get('nom_asso'), date('Y-m-d H')."h".date('i'));	

		return CSV::toODS($nom_fichier, $res, $champs);
	}


	static public function deleteSession($id_session_comptage)
	{
	        $db = DB::getInstance();

	        # Suppression des comptages
	        $db->delete('plugin_comptages_comptages', $db->where('id_session_comptage', $id_session_comptage));		

		# Suppression des types
	        $db->delete('plugin_comptages_types', $db->where('id_session_comptage', $id_session_comptage));		

		# Suppression de la session
	        $db->delete('plugin_comptages_sessions', $db->where('id_session_comptage', $id_session_comptage));		

	}


}
