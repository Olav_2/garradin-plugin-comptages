{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

<p>Créér un comptage</p>

<br>

<form method="post" action="{$self_url}">
    <fieldset>
	{form_errors}
        <legend>Informations sur les caracteristiques du comptage</legend>
        <dl>
                <dt>Description précise du lieu où s'effectue le comptage (adresse, coordonnées GPS, ...) :</dt>
		<dd><input type="text" size="100" name="lieu" ></input></dd>

                <dt>Commentaires :</dt>
		<dd><textarea id="commentaires" name="commentaires" cols="70" rows="5"></textarea></dd>
	</fieldset>

	<fieldset>


	</dl>
        <legend>Types d'éléments à compter :</legend>
	<dl>
	    
            {foreach from=$types_comptages item="type_comptage" key="key"}
		<dt>Type numéro {$key} :</dt>
		<dd><input type="text" size="100" name="T{$key}" value="{$type_comptage}"></input></dd>		
            {/foreach}		


        </dl>
    </fieldset>

	<p class="submit">
    	<input type="submit" name="save" value="Enregistrer &rarr;" />
	</p>

	<p class="submit">
    	<input type="submit" name="saveandcount" value="Enregistrer &rarr; et compter 🤖" />
	</p>

</form>


{include file="admin/_foot.tpl"}
