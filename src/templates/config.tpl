{include file="admin/_head.tpl" title="Configuration — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

{form_errors}

<form method="post" action="{$self_url}">

    <fieldset>
        <legend>Configuration des types d'éléments à compter proposés par défaut</legend>
        <dl>
            <dt>
                <label>
            		{foreach from=$types_comptages item="type_comptage" key="key"}
				<dt>Type numéro {$key} :</dt>
				<dd><input type="text" size="100" name="T{$key}" value="{$type_comptage}"></input></dd>		
		        {/foreach}	
                </label>
            </dt>
        </dl>
    </fieldset>

    <p class="submit">
        <input type="submit" name="save" value="Enregistrer &rarr;" />
    </p>
</form>

{include file="admin/_foot.tpl"}
