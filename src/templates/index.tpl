{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

<p>Liste des comptages</p>
<br>

<table class="list">
	<thead>
		<tr>
			<th>Numéro</th>
			<th>Lieu du comptage</th>
			<th>Membre "compteur"</th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$liste_des_comptages item="comptage"}
		<tr>

			{if ($userid == $comptage.id_membre_compteur)}
				<th>{$comptage.id_session_comptage}</th>
				<th>{$comptage.lieu}</th>
				<th>				
				{if $session->canAccess($session::SECTION_USERS, $session::ACCESS_READ)}
					<a href="{$admin_url}membres/fiche.php?id={$comptage.id_membre_compteur}" class="icn">{$comptage.nom_membre}</a>
				{else}
					{$comptage.nom_membre}
				{/if}
				</th>
			{else}
				<td>{$comptage.id_session_comptage}</td>
				<td>{$comptage.lieu}</td>
				<td>
				{if $session->canAccess($session::SECTION_USERS, $session::ACCESS_READ)}
					<a href="{$admin_url}membres/fiche.php?id={$comptage.id_membre_compteur}" class="icn">{$comptage.nom_membre}</a>
				{else} 
					{$comptage.nom_membre}
				{/if}
				</td>
			{/if}

			<td class="actions">
				{if ($userid == $comptage.id_membre_compteur & $droit_ecriture)}
					<a href="interface_comptage.php?id_session_comptage={$comptage.id_session_comptage}" class="icn" title="Compter">🤖</a>
				{/if}
			</td>
			<td class="actions">
				<a href="consulter_comptage.php?id_session_comptage={$comptage.id_session_comptage}" class="icn" title="Consulter">&#128200;</a>
			</td>
			<td class="actions">
				<a href="./?id_session_comptage={$comptage.id_session_comptage}&export=rawods" class="icn" title="Exporter">&#128188;</a>
			</td>

			<td class="actions">
				{if ($userid == $comptage.id_membre_compteur & $droit_ecriture) || $droit_admin}
					<a href="modifier_session.php?id_session_comptage={$comptage.id_session_comptage}" class="icn" title="Modifier">✎</a>
				{/if}
			</td>

			<td class="actions">
				{if ($userid == $comptage.id_membre_compteur & $droit_ecriture) || $droit_admin}
					<a href="supprimer_session.php?id_session_comptage={$comptage.id_session_comptage}" class="icn" title="Supprimer">🗑️</a>
				{/if}
			</td>

		</tr>
		{/foreach}
	</tbody>
</table>

{if ($droit_ecriture)}
	<form method="get" action=creer_session.php>
	    	<input type="submit" name="creer_session" value="Ajouter un comptage &rarr;" />
	</form>
{/if}

{include file="admin/_foot.tpl"}
