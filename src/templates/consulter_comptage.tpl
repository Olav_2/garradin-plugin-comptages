{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

<h1>Analyse des données</h1>
<br>

<fieldset><legend>Description</legend>
	<dl class="describe">

	<dt>Numéro du comptage : </dt>
	<dd>{$session_comptage.id_session_comptage}</dd>

	<dt>Date de création du comptage : </dt>
	<dd>{$session_comptage.timestamp_date_creation}</dd>

	<dt>Lieu :</dt>
	<dd>{$session_comptage.lieu}</dd>

	<dt>Commentaires :</dt>
	<dd>{$session_comptage.commentaires|escape:raw}</dd>

	</dl>
</fieldset>

<br>

<fieldset><legend>Totaux</legend>
<table class="list">

	<thead class="userOrder">
		<tr>
			<td>Début</td>
			<td>Fin</td>
			<td align="center">Durée secondes</td>
			<td align="center">Durée</td>
			{foreach from=$types_comptages item="type_comptage"}
			<td align="center">{$type_comptage.description}</td>
			{/foreach}
			<td align="center">Total</td>
		<tr>	
		
	</thead>

	<tbody>
		{foreach from=$totaux item="total"}
		{if $total.datetime_debut == "Total"}</tbody><tfoot>{/if}
		<tr>
			<td>
				{$total.datetime_debut|escape}
			</td>
			<td>
				{$total.datetime_fin|escape}
			</td>
			<td align="center">
				{$total.duree_secondes|escape}
			</td>
			<td align="center">
				{$total.duree_HMS|escape}
			</td>

			{foreach from=$types_comptages item="type_comptage"}
				<td  align="center">
					{$total.totaux[$type_comptage.type_comptage]|escape}
				</td>
			{/foreach}
			<td align="center">
				<b>{$total.total_horodatages|escape}</b>
			</td>
		</tr>
		{/foreach}
	
	</tfoot>

</table>

</fieldset>

<br>

<fieldset><legend>Répartition en pourcentages</legend>
<table class="list">

	<thead class="userOrder">
		<tr>
			<td>Début</td>
			<td>Fin</td>
			<td align="center">Durée secondes</td>
			<td align="center">Durée</td>
			{foreach from=$types_comptages item="type_comptage"}
			<td align="center">{$type_comptage.description}</td>
			{/foreach}
		<tr>	
		
	</thead>

	<tbody>
		{foreach from=$totaux item="total"}
		{if $total.datetime_debut == "Total"}</tbody><tfoot>{/if}		
		<tr>
			<td>
				{$total.datetime_debut|escape}
			</td>
			<td>
				{$total.datetime_fin|escape}
			</td>
			<td align="center">
				{$total.duree_secondes|escape}
			</td>
			<td align="center">
				{$total.duree_HMS|escape}
			</td>

			{foreach from=$types_comptages item="type_comptage"}
				<td  align="center">
					{$total.pourcentages[$type_comptage.type_comptage]|escape}%
				</td>
			{/foreach}

		</tr>
		{/foreach}
	
	</tfoot>

</table>

</fieldset>


<br>


<fieldset><legend>Fréquences moyennes par heure</legend>
<table class="list">

	<thead class="userOrder">
		<tr>
			<td>Début</td>
			<td>Fin</td>
			<td align="center">Durée secondes</td>
			<td align="center">Durée</td>
			{foreach from=$types_comptages item="type_comptage"}
			<td align="center">{$type_comptage.description}</td>
			{/foreach}
			<td align="center">Total</td>
		<tr>	
		
	</thead>

	<tbody>
		{foreach from=$totaux item="total"}
		{if $total.datetime_debut == "Total"}</tbody><tfoot>{/if}		
		<tr>
			<td>
				{$total.datetime_debut|escape}
			</td>
			<td>
				{$total.datetime_fin|escape}
			</td>
			<td align="center">
				{$total.duree_secondes|escape}
			</td>
			<td align="center">
				{$total.duree_HMS|escape}
			</td>

			{foreach from=$types_comptages item="type_comptage"}
				<td  align="center">
					{$total.taux_horaires[$type_comptage.type_comptage]|escape}/h
				</td>
			{/foreach}

			<td align="center">
				<b>{$total.taux_horaire_total|escape}/h</b>
			</td>

		</tr>
		{/foreach}
	
	</tfoot>

</table>

</fieldset>




{include file="admin/_foot.tpl"}
