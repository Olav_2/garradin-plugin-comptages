{include file="admin/_head.tpl" title="Supprimer un comptage" current="membres"}

{form_errors}

<form method="post" action="{$self_url}">

    <fieldset>
        <legend>Supprimer le comptage numéro {$id_session_comptage} ?</legend>
	{if strcmp($supp_session_autre_membre,"oui") == 0}
        <p class="alert">
	<strong>Attention</strong> : 
            Ce comptage appartient à un autre membre, mais vous pouvez néanmoins le supprimer car vous avez des droits d'aministrateur.
        </p>
	{/if}

        <h3 class="warning">
            Êtes-vous sûr de vouloir supprimer le comptage numéro {$id_session_comptage} ?
        </h3>
        <p class="alert">
            <strong>Attention</strong> : cette action est irréversible
        </p>
    </fieldset>

    <p class="submit">
        {csrf_field key="delete_session_"|cat:$id_session_comptage}
        <input type="submit" name="delete" value="Supprimer &rarr;" />
    </p>

</form>

{include file="admin/_foot.tpl"}
