{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

<p>Modifier un comptage</p>

<br>

<form method="post" action="{$self_url}">
    <fieldset>
	{form_errors}
        <legend>Informations sur les caracteristiques du comptage</legend>

	{if strcmp($modif_session_autre_membre,"oui") == 0}
        <p class="alert">
	<strong>Attention</strong> : 
            Ce comptage appartient à un autre membre, mais vous pouvez néanmoins le modifier car vous avez des droits d'aministrateur.
        </p>
	{/if}

        <dl>
                <dt>Description précise du lieu où s'effectue le comptage (adresse, coordonnées GPS, ...) :</dt>
		<dd><input type="text" size="100" name="lieu" value="{$session_comptage.lieu}"></input></dd>

                <dt>Commentaires :</dt>
		<dd><textarea id="commentaires" name="commentaires" cols="70" rows="5">{$session_comptage.commentaires}</textarea></dd>
	</fieldset>

	<fieldset>


	</dl>
        <legend>Types d'éléments à compter :</legend>
	<dl>
	    
	{foreach from=$types_comptages item="type_comptage"}
		<dt>Type numéro {$type_comptage.type_comptage} :</dt>
		<dd><input type="text" size="100" name="T{$type_comptage.type_comptage}" value="{$type_comptage.description}"></input></dd>		
	{/foreach}		


        </dl>
    </fieldset>

	<p class="submit">
    	<input type="submit" name="save" value="Enregistrer &rarr;" />
	</p>

</form>


{include file="admin/_foot.tpl"}
