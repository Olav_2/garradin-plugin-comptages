<?php

namespace Garradin;

$db = DB::getInstance();

$db->exec(<<<EOF

    CREATE TABLE IF NOT EXISTS plugin_comptages_sessions
    (
	timestamp_date_creation TEXT, 

	id_session_comptage INTEGER,

	id_membre_compteur INTEGER,

	lieu TEXT,

	commentaires TEXT

    );

    CREATE TABLE IF NOT EXISTS plugin_comptages_types
    (

	id_session_comptage INTEGER,

	type_comptage INTEGER,

	description TEXT

    );

    CREATE TABLE IF NOT EXISTS plugin_comptages_comptages
    (
	timestamp_comptage TEXT, 

	id_session_comptage INTEGER,

	type_comptage INTEGER


    );

EOF
);

