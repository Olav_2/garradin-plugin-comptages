<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_DOCUMENTS, $session::ACCESS_READ);

$user = $session->getUser();

$userid = $user->id;

if ($user->perm_documents < $session::ACCESS_ADMIN) {
	$droit_admin = false;
} else {
	$droit_admin = true;
}

if ($user->perm_documents < $session::ACCESS_READ) {
	$droit_ecriture = false;
} else {
	$droit_ecriture = true;
}

if (qg('export') == 'rawods')
{
	$comptages->export_rawdata_ODS(qg('id_session_comptage'));

	exit;
}

$liste_des_comptages = $comptages->getList(); 

$tpl->assign('userid', $userid);

$tpl->assign('droit_admin', $droit_admin);

$tpl->assign('droit_ecriture', $droit_ecriture);

$tpl->assign('liste_des_comptages', $liste_des_comptages);

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');
