<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_DOCUMENTS, $session::ACCESS_READ);

$user = $session->getUser();

qv(['id_session_comptage' => 'required|numeric']);

$id_session_comptage = qg('id_session_comptage');

$session_comptage = $comptages->getSession($id_session_comptage);

if (!$session_comptage)
{
	throw new UserException("Ce comptage n'existe pas.");
}

$modif_session_autre_membre = "non";
if ($session_comptage->id_membre_compteur != $user->id)
{
	if ($user->perm_documents  < $session::ACCESS_ADMIN) {
		throw new UserException("Vous ne pouvez pas modifier un comptage d'un autre membre");
	} else {
		$modif_session_autre_membre = "oui";
	}
}


$types_comptages = $comptages->getTypesComptages($id_session_comptage);



if (f('save'))
{
	try {

		$data = [
		'lieu'    	=>  f('lieu'),
		'commentaires'  =>  f('commentaires')
		];

		$types_comptages = [];

		foreach ($_POST as $param => $valeur) {
			if (substr($param, 0, 1) == "T") {
				$clef = substr($param, 1 - strlen($param));
				$types_comptages[$clef] = $valeur;
			}
		}

		$comptages->editSession($id_session_comptage, $data, $types_comptages);

		utils::redirect(utils::plugin_url());

	}

	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
}

$tpl->assign('modif_session_autre_membre', $modif_session_autre_membre);

$tpl->assign('session_comptage', $session_comptage);

$tpl->assign('types_comptages', $types_comptages);

$tpl->display(PLUGIN_ROOT . '/templates/modifier_session.tpl');
