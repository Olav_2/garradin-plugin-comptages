<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_DOCUMENTS, $session::ACCESS_READ);
$user = $session->getUser();

qv(['id_session_comptage' => 'required|numeric']);

$id_session_comptage = qg('id_session_comptage');

$session_comptage = $comptages->getSession($id_session_comptage);

if (!$session_comptage)
{
	throw new UserException("Ce comptage n'existe pas.");
}

$supp_session_autre_membre = "non";

if ($session_comptage->id_membre_compteur != $user->id)
{
	if ($user->perm_documents  < $session::ACCESS_ADMIN) {
		throw new UserException("Vous ne pouvez pas supprimer un comptage d'un autre membre");
	} else {
		$supp_session_autre_membre = "oui";
	}
}



if (f('delete'))
{
    $form->check('delete_session_'.$id_session_comptage);

    if (!$form->hasErrors())
    {
        try {
	        $comptages->deleteSession($id_session_comptage);
		utils::redirect(utils::plugin_url());
        }
        catch (UserException $e)
        {
		$form->addError($e->getMessage());
        }
    }
}


$tpl->assign('supp_session_autre_membre', $supp_session_autre_membre);

$tpl->assign('id_session_comptage', $id_session_comptage);

$tpl->display(PLUGIN_ROOT . '/templates/supprimer.tpl');
