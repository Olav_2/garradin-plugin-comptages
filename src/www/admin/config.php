<?php

namespace Garradin;

$session->requireAccess($session::SECTION_CONFIG, $session::ACCESS_ADMIN);


if (f('save'))
{
	try {
		$types_comptages = [];

		foreach ($_POST as $param => $valeur) {
			if (substr($param, 0, 1) == "T") {
				$clef = substr($param, 1 - strlen($param));
				$types_comptages[$clef] = $valeur;
			}
		}

    		$plugin->setConfig('types_comptages', $types_comptages);
		utils::redirect(utils::plugin_url());
	}
	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
}


$types_comptages = $plugin->getConfig("types_comptages");

$tpl->assign('types_comptages',$types_comptages);

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');
