<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_DOCUMENTS, $session::ACCESS_READ);

$types_comptages = $plugin->getConfig("types_comptages");




if (f('save') || f('saveandcount'))
{
	try {

		$data = [
		'lieu'    	=>  f('lieu'),
		'commentaires'  =>  f('commentaires')
		];

		$types_comptages = [];

		foreach ($_POST as $param => $valeur) {
			if (substr($param, 0, 1) == "T") {
				$clef = substr($param, 1 - strlen($param));
				$types_comptages[$clef] = $valeur;
			}
		}

		$comptages->addSession($data, $types_comptages);

		if (f('saveandcount')) {
			utils::redirect(utils::plugin_url() . "interface_comptage.php?id_session_comptage=".$data["id_session_comptage"]);
		} else {
			utils::redirect(utils::plugin_url());
		}

	}

	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
}






$tpl->assign('types_comptages', $types_comptages);

$tpl->display(PLUGIN_ROOT . '/templates/creer_session.tpl');
