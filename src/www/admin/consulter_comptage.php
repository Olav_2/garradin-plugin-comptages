<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$id_session_comptage = qg('id_session_comptage');

$session_comptage = $comptages->getSession($id_session_comptage);

$session_comptage->{'commentaires'} = str_replace("\n",'<br>',$session_comptage->{'commentaires'});

$tpl->assign('session_comptage', $session_comptage);

$types_comptages = $comptages->getTypesComptages($id_session_comptage);
$tpl->assign('types_comptages', $types_comptages);

$horodatages = $comptages->getComptages($id_session_comptage);

if (count($horodatages) == 0) {
	throw new UserException("Ce comptage n'a pas encore été démarré.");
}

$totaux = $comptages->calculerTotaux($types_comptages, $horodatages);

if (! $totaux) 
{
	throw new UserException("Il n'y a pas encore de comptages.");
}

$tpl->assign('totaux', $totaux);

$tpl->display(PLUGIN_ROOT . '/templates/consulter_comptage.tpl');
