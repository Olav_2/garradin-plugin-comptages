<?php

namespace Garradin;

use Garradin\Plugin\comptages\Comptages;

$session->requireAccess($session::SECTION_DOCUMENTS, $session::ACCESS_READ);

$comptages = new Comptages;

$tpl->assign('plugin_css', ['style.css']);

$tpl->assign('plugin_js', ['envoyer.js']);
