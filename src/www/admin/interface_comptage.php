<?php

namespace Garradin;

use Garradin\Membres\Session;

require_once __DIR__ . '/_inc.php';

$id_session_comptage = qg('id_session_comptage');

$session_comptage = $comptages->getSession($id_session_comptage);

$session = Session::getInstance();
$user = $session->getUser();
$id = $user->id;

if ($id != $session_comptage->{'id_membre_compteur'}) {
	throw new UserException("Ce comptage ne vous appartient pas, vous ne pouvez pas l'utiliser.");
}


$types_comptages = $comptages->getTypesComptages($id_session_comptage);

# on envoie un pseudo comptage de type "0" pour enregistrer le timestamp de début de l'intervalle de comptage
$comptages->ajouterComptage($id_session_comptage, "0");

$tpl->assign('id_session_comptage', $id_session_comptage);

$tpl->assign('types_comptages', $types_comptages);

$tpl->assign('position', 'right');

$tpl->display(PLUGIN_ROOT . '/templates/interface_comptage.tpl');
